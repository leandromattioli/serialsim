/**
 * @file lrm.waveform.js
 **/

if (window.lrmWidCount == undefined)
    window.lrmWidCount = 0;

$.widget( "lrm.waveform", {

    options: {
        lblY: undefined,
        border: false,
        scale: 5,        // pixels per simulator ticks
        width: 500,
        height: 30,
        marks: [],       // marks (equivalent of gnuplot ticks),
        data: [ [0,0] ], // data to be plotted [ [x0,y0], [x1,y1]... ]
        canvas: undefined,
        widgetRef: 0
    },
    
    //========================================================================    
    //Creation-related methods
    //========================================================================
    
    /**
     * Creates all DOM structure for this widget
     **/
    _createElements: function() {
        this.element.html('');
        $(this.element).addClass('lrm_waveform');
        var canvas = $('<canvas />');
        canvas.appendTo(this.element);
        this.element.width(this.options.width);
        this.element.height(this.options.height);
        canvas.attr('width', this.options.width);
        canvas.attr('height', this.options.height);
        this.options.canvas = canvas;
        this.plot(0);
    },
    
    //========================================================================
    //Actions
    //========================================================================
    
    _getX: function(x) {
        return x * this.options.scale + 1;
    },
    
    _getY: function(y) {
        var invY = y * (this.options.height - 8) + 4;
        return this.options.height - invY; 
    },
    
    setData: function(data) {
        this.options.data = data;
        this.clear();
        this.plot();
    },
    
    //========================================================================    
    //Plot methods
    //========================================================================
    
    plot: function(tickCount) {
        this.clear();
        var ctx = this.options.canvas[0].getContext('2d');
        
        //Pad X to text
        var padX = (this.options.lblY == undefined) ? 0 : 30;
        
        //Border
        if(this.border) {
            ctx.beginPath();
            ctx.lineWidth = 0.5;
            ctx.rect(1,1, this.options.width - 2 - padX, this.options.height - 2);
            ctx.strokeStyle = "#000080";
            ctx.stroke();
        }
        
        ctx.save();
        if(this.options.lblY != undefined) {
            ctx.font="14px sans-serif";
            ctx.textBaseline="top";
            ctx.fillStyle = "blue";
            ctx.fillText(this.options.lblY, 0, 7, padX);
            ctx.translate(padX,0);
        }
        
        //Values
        var d = this.options.data;
        if(!d.length) {
            ctx.restore();
            return;
        }
        
        var x0, y0, x1, y1;
        x1 = this._getX(d[0][0]);
        y1 = this._getY(d[0][1]);
        var i;
        for(i = 1; i <= d.length - 1; i++) {
            x0 = this._getX(d[i - 1][0]);
            x1 = this._getX(d[i][0]);
            y0 = this._getY(d[i - 1][1]);
            y1 = this._getY(d[i][1]);
            //Horizontal line
            ctx.beginPath();
            ctx.strokeStyle = "#000";
            ctx.moveTo(x0, y0);
            ctx.lineTo(x1, y0);
            ctx.stroke();
            
            //Vertical line
            ctx.beginPath();
            if(d[i].length >= 3)
                ctx.strokeStyle = d[i][2];
            ctx.moveTo(x1, y0);
            ctx.lineTo(x1, y1);
            ctx.stroke();
        }
        
        //Hold
        if(tickCount > d[i - 1][0]) {
            ctx.beginPath();
            ctx.strokeStyle = "#000";
            ctx.moveTo(x1, y1);
            ctx.lineTo(this._getX(tickCount), y1);
            ctx.stroke();
        }
        
        ctx.restore();
    },
    
    clear: function() {
        var ctx = this.options.canvas[0].getContext('2d');
        ctx.fillStyle='#FFFFFF';
        ctx.clearRect(0,0, this.options.width, this.options.height);
    },
    
    //========================================================================
    //Inherited methods
    //========================================================================
 
    _create: function() {
        //Unique counter for elements IDs
        lrmWidCount++;
        this.options.widgetRef = lrmWidCount;
        this._createElements();
    },
 
    destroy: function() {
        this.element.html('');
        $.Widget.prototype.destroy.call( this ); //base destroy
    }

});
