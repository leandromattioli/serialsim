String.prototype.rjust = function (char, max) {
    var newStr = this;
    while(newStr.length < max)
        newStr = char + newStr;
    return newStr;
}
