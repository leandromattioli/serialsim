/**
 * @file lrm.byteEntry.js
 * @depends lrm.utils.js
 **/

if (window.lrmWidCount == undefined)
    window.lrmWidCount = 0;

$.widget( "lrm.digClock", {

    options: { 
        totalms: 0,
        mins: 0,
        secs: 0,
        ms: 0,
        widgetRef: 0,
        imgs: [],
        imgPrefix: './img/digclock/'
    },
    
    //========================================================================    
    //Creation-related methods
    //========================================================================
    
    /**
     * Creates all DOM structure for this widget
     **/
    _createElements: function() {
        this.element.html('');
        for(var i = 0; i < 7; i++) {
            var img = $('<img />');
            img.attr('src', this.options.imgPrefix + '0.svg');
            img.appendTo(this.element);
            this.options.imgs.push(img);
            //Add : image
            if(i == 1 || i == 3) {
                
            }
                
        }
    },
    
    //========================================================================
    //Utility Functions
    //========================================================================
    
    
    //========================================================================
    //Actions
    //========================================================================
    
    setMins: function(val) {
    
    },
    
    setSecs: function(val) {
    
    },
    
    setMs: function() {
        this.preload(this.options.value >> 1);
    },
    
    //========================================================================
    //Inherited methods
    //========================================================================
 
    _create: function() {
        //Unique counter for elements IDs
        lrmWidCount++;
        this.options.widgetRef = lrmWidCount;
        this._createElements();
    },
 
    destroy: function() {
        this.element.html('');
        $.Widget.prototype.destroy.call( this ); //base destroy
    }

});
