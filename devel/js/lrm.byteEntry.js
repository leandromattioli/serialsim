/**
 * @file lrm.byteEntry.js
 * @depends lrm.utils.js
 **/

if (window.lrmWidCount == undefined)
    window.lrmWidCount = 0;

$.widget( "lrm.byteEntry", {

    options: { 
        value: 127, 
        widgetRef: 0,
        editable: true,
        dec: undefined,
        hex: undefined,
        bin: undefined,
        upBtn: undefined,
        downBtn: undefined,
    },
    
    //========================================================================    
    //Creation-related methods
    //========================================================================
    
    /**
     * Creates a paragraph with a label and an input 
     **/
    _inputHelper: function(type, idPrefix) {
        idPrefix += '_' + type;
        var p = $('<p />');
        //Label
        var label = $('<label />');
        label.attr('for', idPrefix + 'Input');
        label.html(type + ': &nbsp;');
        label.appendTo(p);
        //Input
        var input = $('<input />');
        input.attr('id', idPrefix + 'Input');
        input.attr('type', 'text');
        input.attr('value', '0');
        input.appendTo(p);
        //Complete par
        this.element.append(p);
    },
    
    /**
     * Creates all DOM structure for this widget
     **/
    _createElements: function() {
        var idPrefix = 'lrm_be_' + this.options.widgetRef;
        $(this.element).addClass('lrm_be');
        //Creating DOM elements
        this._inputHelper('DEC', idPrefix);
        this._inputHelper('HEX', idPrefix);
        this._inputHelper('BIN', idPrefix);
        if(this.options.editable) {
            //Spin buttons
            var p = $('<p />');
            p.addClass('lrm_be_buttons');
            var upBtn = $('<button />');
            upBtn.attr('id', idPrefix + '_UpBtn');
            upBtn.attr('value', '+1');
            upBtn.text('+1');
            upBtn.attr('type', 'button');
            upBtn.appendTo(p);
            upBtn.after(' ');
            var downBtn = $('<button />');
            downBtn.attr('id', idPrefix + '_DownBtn');
            downBtn.attr('value', '-1');
            downBtn.text('-1');
            downBtn.attr('type', 'button');
            downBtn.appendTo(p);
            this.element.append(p);
            this.options.upBtn = $('#' + idPrefix + '_UpBtn');
            this.options.downBtn = $('#' + idPrefix + '_DownBtn');
        }
        //Creating references
        this.options.dec = $('#' + idPrefix + '_DECInput');
        this.options.hex = $('#' + idPrefix + '_HEXInput');
        this.options.bin = $('#' + idPrefix + '_BINInput');

        //CSS
        this.options.dec.addClass('lrm_be_entry');
        this.options.hex.addClass('lrm_be_entry');
        this.options.bin.addClass('lrm_be_entry');
        //Metadata
        this.options.dec.data('base', 10);
        this.options.hex.data('base', 16);
        this.options.bin.data('base', 2);
    },
    
    //========================================================================    
    //Utility functions
    //========================================================================
    
    _isEmpty: function(val) {
        return val.replace(/_ /g, '').length == 0;
    },
    
    
    _updateSensivity: function() {
        if(this.options.editable)
            $(this.element).find('.lrm_be_entry').prop('disabled', false);
        else
            $(this.element).find('.lrm_be_entry').prop('disabled', true);
    },
    
    
    //========================================================================    
    //Events handlers
    //========================================================================
    
    _updateValue: function(entry, base) {
        var strNumber = entry.val().replace(/[_ ]/g, '');
        var number = parseInt(strNumber, base);
        if(isNaN(number))
            number = 0;
        number = (number > 255) ? 255 : number;
        this.options.value = number;
        this._update();
    },
    
    _onKeyUp: function(evt) {
        if(evt.keyCode == 13) { //Enter
            var element = $(evt.currentTarget);
            element.parent().next().find('input').focus();
        }
    },
    
    _onFocus: function(evt) {
        var element = $(evt.currentTarget);
        element.addClass('lrm_be_editing');
    },
    
    _onBlur: function(evt) {
        var element = $(evt.currentTarget);
        var base = element.data('base');
        if(this._isEmpty(element.val()))
            element.val('0');
        this._updateValue(element, base);
        element.removeClass('lrm_be_editing');
    },
    
    _onComplete: function(evt) {
        var element = $(document.activeElement);
        var base = element.data('base');
        this._updateValue(element, base);
    },
    
    _configEntries: function() {
        //Masks
        this.options.dec.mask('9?99', {
            completed: $.proxy(this._onComplete, this)        
        });
        $.mask.definitions['h'] = "[A-Fa-f0-9]";
        this.options.hex.mask('h?h', {
            completed: $.proxy(this._onComplete, this)
        });
        $.mask.definitions['b'] = '[01]';
        this.options.bin.mask('bbbb bbbb', {
            completed: $.proxy(this._onComplete, this)
        });
        //Change events
        $(this.element).find('.lrm_be_entry').on('keyup', 
            $.proxy(this._onKeyUp, this));
        $(this.element).find('.lrm_be_entry').on('focus', 
            $.proxy(this._onFocus, this));
        $(this.element).find('.lrm_be_entry').on('blur', 
            $.proxy(this._onBlur, this));
    },
    
    _configButtons: function() {
        this._on(this.options.upBtn, {
            "click": function(event) {
                this._increment();
            }
        });
        this._on(this.options.downBtn, {
            "click": function(event) {
                this._decrement();
            }
        });
    },
    
    //========================================================================
    //Actions
    //========================================================================
    
    _increment: function() {
        if(this.options.value >= 255)
            return;
        this.options.value++;
        this._update();
    },
    
    _decrement: function() {
        if(this.options.value <= 0)
            return;
        this.options.value--;
        this._update();
    },
    
    getValue: function() {
        return this.options.value;
    },
    
    setValue: function(value) {
        this.options.value = value;
        this._update();
    },
    
    //========================================================================
    //Inherited methods
    //========================================================================
 
    _create: function() {
        //Unique counter for elements IDs
        lrmWidCount++;
        this.options.widgetRef = lrmWidCount;
        
        this._createElements();
        this._configEntries();
        if(this.options.editable) {
            this._configButtons();
        }
        
        this._update();
        this._updateSensivity();
    },
 
    _setOption: function( key, value ) {
        this.options[ key ] = value;
        this._update();
        this._updateSensivity();
    },
 
    _update: function() {
        this.options.dec.val(this.options.value);
        this.options.hex.val(this.options.value.toString(16).toUpperCase());
        var binString = this.options.value.toString(2);
        binString = binString.rjust('0', 8);
        binString = binString.substr(0, 4) + " " + binString.substr(4);
        this.options.bin.val(binString);
    },
 
    destroy: function() {
        this.element.html('');
        $.Widget.prototype.destroy.call( this ); //base destroy
    }

});
