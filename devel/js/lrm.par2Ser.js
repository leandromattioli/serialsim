/**
 * @file lrm.par2ser.js
 * @depends lrm.utils.js
 **/

if (window.lrmWidCount == undefined)
    window.lrmWidCount = 0;

$.widget( "lrm.par2Ser", {

    options: { 
        value: 255, 
        widgetRef: 0,
        bits: undefined
    },
    
    //========================================================================    
    //Creation-related methods
    //========================================================================
    
    /**
     * Creates all DOM structure for this widget
     **/
    _createElements: function() {
        this.element.html('');
        this.options.bits = [];
        var binString = this.getBinString();
        $(this.element).addClass('lrm_par2ser');
        var table = $('<table />')
        //Phantom row
        var tr = $('<tr />');
        tr.addClass('lrm_phantomRow')
        var td;
        for(var i = 0; i < 8; i++) {
            td = $('<td>&darr;</td>');
            td.appendTo(tr);
        }
        td = $('<td>&nbsp;</td>');
        td.appendTo(tr);
        tr.appendTo(table);
        //Value row
        tr = $('<tr />');
        for(var i = 0; i < 8; i++) {
            td = $('<td />');
            td.html(binString[i]);
            this.options.bits.push(td);
            td.appendTo(tr);
        }
        td = $('<td>&rarr;</td>');
        td.addClass('lrm_par2ser_output');
        td.appendTo(tr);
        tr.appendTo(table);
        table.appendTo(this.element);        
    },
    
    //========================================================================
    //Utility Functions
    //========================================================================
    getBinString: function() {
        var binString = this.options.value.toString(2);
        return binString.rjust('0', 8);
    },
    
    //========================================================================
    //Actions
    //========================================================================
    
    preload: function(value) {
        this.options.value = value;
        var binString = this.getBinString();
        for(var i = 0; i < 8; i++)
            this.options.bits[i].html(binString[i]);
    },
    
    clock: function() {
        this.preload(this.options.value >> 1);
    },
    
    getValue: function() {
        return this.options.value;
    },
    
    //========================================================================
    //Inherited methods
    //========================================================================
 
    _create: function() {
        //Unique counter for elements IDs
        lrmWidCount++;
        this.options.widgetRef = lrmWidCount;
        this._createElements();
    },
 
    destroy: function() {
        this.element.html('');
        $.Widget.prototype.destroy.call( this ); //base destroy
    }

});
