/* Clock Signal 
   Property:
      semiPeriod: number of ticks that changes clock value
   @TODO: Rising time; falling time
*/

function Clock() {
    this.semiPeriod = 10;
}

Clock.prototype.getValue = function(elapsedTicks) {
    return Math.floor(elapsedTicks / this.semiPeriod) % 2;
}
