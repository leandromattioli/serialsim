//Generic Bin View

function BinView(lblNode) {
    this._on = false;
    this._lblNode = lblNode;
    this._lblNode.html('0');
}

BinView.prototype.getValue = function() {
    return this._on;
}

BinView.prototype.setValue = function(newValue) {
    this._on = newValue;
    if(newValue == true)
        this._lblNode.html('1');
    else
        this._lblNode.html('0');
}

// LED

function ViewLED(lblNode) {
    BinView.call(this, lblNode);
};

ViewLED.prototype = Object.create(BinView.prototype);

ViewLED.prototype.draw = function(ctx) {
    if(!this._on)
        return;
    ctx.beginPath();
    ctx.moveTo(331, 170);
    ctx.lineTo(368, 170);
    ctx.lineTo(350, 132);
    ctx.closePath();
    ctx.fillStyle = '00C';
    ctx.fill();
}

// Transistor

function ViewTransistor(lblNode) {
    BinView.call(this, lblNode);
};

ViewTransistor.prototype = Object.create(BinView.prototype);

ViewTransistor.prototype.draw = function(ctx) {
    if(!this._on)
        return;
    ctx.beginPath();
    ctx.arc(641,150,37.5,0,2*Math.PI);
    ctx.strokeStyle = '00C';
    ctx.fillStyle = "transparent";
    ctx.lineWidth = 4;
    ctx.stroke();
}

// Barrier

function ViewBarrier(canvasNode) {
    this._on = false;
    this._button = true;
    this._imgPattern = new Image();
    this._imgPattern.src = 'img/serial_barreira.svg';
}

ViewBarrier.prototype.setValue = function(newVal) {
    this._on = newVal;
}

ViewBarrier.prototype.getValue = function() {
    return this._on;
}

ViewBarrier.prototype.highlightButton = function() {
    this._button = true;
}

ViewBarrier.prototype.unhighlightButton = function() {
    this._button = false;
}

ViewBarrier.prototype.isMyRegion = function(evt) {
    var offset = $(evt.target).offset();
    var x = evt.clientX - offset.left;
    var y = evt.clientY - offset.top;
    return (x > 400 && x < 600 && y < 40);
}

ViewBarrier.prototype.draw = function(ctx) {
    if(this._on) {
        var pattern = ctx.createPattern(this._imgPattern, 'repeat');
        ctx.beginPath();
        ctx.fillStyle = pattern;
        ctx.fillRect(490, 80, 20, 140);
    }
    if(this._button) {
        ctx.beginPath();
        ctx.fillStyle = "rgba(150, 150, 150, 0.2)";
        ctx.fillRect(401, 1, 199, 39);
        ctx.moveTo(500, 70);
        ctx.lineTo(470, 40);
        ctx.lineTo(530, 40);
        ctx.closePath();
        ctx.fill();
    }
}

