/*
    * Tempo: número inteiro (nro. de ticks) * fator de escala
    * Widget Relógio Digital:
     - usar displays de 7 segmentos
     - widget display 7 segmentos (embedded font?)
    * Widget Waveform
     - marcas (ticks) no eixo x;
    * Simulador:
       - verificar barreira no meio de transmissão (Tx != Rx)
       - receptor deve esperar pelos bits de controle (START e STOP)
*/

function Simulator() {
    //Simulator Controls
    this.btnPlayPause = $('#cmd_playPause');
    this.btnPlayPause.on('click', $.proxy(this.playPause, this));
    this.btnStop = $('#cmd_stop');
    this.btnStop.on('click', $.proxy(this.stop, this));
    this.btnStep = $('#cmd_step');
    this.btnStep.on('click', $.proxy(this.step, this));
    this.delay = $('#sim_delay').slider({
        min: 0,
        max: 700,
        value: 300,
        slide: function( event, ui ) {
            $("#sim_delayVal").html('' + ui.value + ' ms');
        }
    });
    //Serial Widgets
    this.byteEntry1 = $('#byte_entry1').byteEntry({value: 137});
    this.byteEntry2 = $('#byte_entry2').byteEntry({
        editable: false, value: 0
    });
    this.par2Ser = $('#par2ser_1').par2Ser();
    this.ser2Par = $('#ser2par_1').ser2Par();
    this.viewLED = new ViewLED($('#lblTx'));
    this.viewTransistor = new ViewTransistor($('#lblRx'));
    this.viewBarrier = new ViewBarrier();
    //Waveforms
    this.clkWave = $('#clkWave').waveform({
        width: 480, scale: 2, lblY: 'CLK'
    });
    this.txWave = $('#txWave').waveform({
        width: 480, scale: 2, lblY: 'TX' 
    });
    this.rxWave = $('#rxWave').waveform({
        width: 480, scale: 2, lblY: 'RX'
    });
    //Overlay
    this.overlay = $('#overlay');
    this.overlay.on('click', $.proxy(this.canvasClick, this));
    this.overlay.on('mousemove', $.proxy(this.canvasMouseMove, this));
    this.overlay.on('mouseleave', $.proxy(this.canvasMouseLeave, this));
    //Objects
    this.clock = new Clock();
    this.macroStep = 10;
    this.end = 200;
    this.stop();
}

//=============================================================================
//Commands and Status
//=============================================================================

Simulator.prototype.step = function() {
    //Finding tickCount associated with next clock edge
    var tmp = this.tickCount + this.macroStep;
    this.tickCount = tmp - (tmp % this.macroStep);
    //Running step
    this.microstep();
};

Simulator.prototype.play = function() {
    this.delay.slider('disable');
    this.simIntervalID = window.setInterval(
        $.proxy(this.microstep, this), this.delay.slider('value')
    );
    this.canvasIntervalID = window.setInterval(
        $.proxy(this.canvasExpose, this), 10
    );
}

Simulator.prototype.pause = function() {
    this.delay.slider('enable');
    window.clearInterval(this.simIntervalID);
    window.clearInterval(this.canvasIntervalID);
    this.simIntervalID = undefined;
}

Simulator.prototype.isPlaying = function() {
    return this.simIntervalID != undefined;
}

Simulator.prototype.playPause = function() {
    if(this.simIntervalID == undefined && this.tickCount < this.end)
        this.play();
    else
        this.pause();
}

Simulator.prototype.stop = function() {
    this.pause();
    this.tickCount = 0;
    this.clkWaveData = [ [0,0] ];
    this.clkWave.waveform('setData', this.clkWaveData);
    this.txWaveData = [];
    this.txWave.waveform('setData', this.txWaveData);
    this.rxWaveData = [];
    this.rxWave.waveform('setData', this.rxWaveData);
    this.clearHighlights();
    this.viewLED.setValue(false);
    this.viewTransistor.setValue(false);
    this.viewBarrier.unhighlightButton();
    this.canvasExpose();
    this.updateSensivity();
}


//=============================================================================
// Overlay
//=============================================================================

Simulator.prototype.canvasClick = function(evt) {
    if(this.simIntervalID == undefined)
        return;
    if(this.viewBarrier.isMyRegion(evt)) {
        if(this.tickCount < 2 *this.macroStep) {
            alert("Nesta versão o meio não pode ser bloqueado antes do START bit");
            return;
        }
        this.viewBarrier.setValue(!this.viewBarrier.getValue());
    }
    //If medium is blocked RX becomes or continues at low
    if(this.viewBarrier.getValue()) {
        this.rxWaveData.push([this.tickCount, 0]);
        this.viewTransistor.setValue(false);
    }
    //If medium is released and LED is still on...  
    else if(this.txWaveData[this.txWaveData.length - 1][1] == 1) {
        this.rxWaveData.push([this.tickCount, 1]);
        this.viewTransistor.setValue(true);
    }
}

Simulator.prototype.canvasMouseMove = function(evt) {
    if(this.viewBarrier.isMyRegion(evt))
        this.viewBarrier.highlightButton();
    else
        this.viewBarrier.unhighlightButton();
}

Simulator.prototype.canvasMouseLeave = function(evt) {
    this.viewBarrier.unhighlightButton();
}

Simulator.prototype.canvasExpose = function() {
    var canvas = this.overlay[0]
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    this.viewLED.draw(ctx);
    this.viewTransistor.draw(ctx);
    this.viewBarrier.draw(ctx);
}

//=============================================================================
//Actions
//=============================================================================

Simulator.prototype.updateSensivity = function() {
    if(this.tickCount > this.end - this.macroStep)
        this.btnStep.prop('disabled', true);
    else
        this.btnStep.prop('disabled', false);
}

Simulator.prototype.sendOne = function() {
    this.txWaveData.push([this.tickCount, 1]);
    this.viewLED.setValue(true);
    if(!this.viewBarrier.getValue()) {
        this.viewTransistor.setValue(true);
        this.rxWaveData.push([this.tickCount, 1]);
    }
    else {
        this.viewTransistor.setValue(false);
        this.rxWaveData.push([this.tickCount, 0]);
    }
}

Simulator.prototype.sendZero = function() {
    this.txWaveData.push([this.tickCount, 0]);
    this.rxWaveData.push([this.tickCount, 0]);
    this.viewLED.setValue(false);
    this.viewTransistor.setValue(false);
}

Simulator.prototype.clearHighlights = function() {
    $('p').removeClass('highlight');
    $('td').removeClass('highlight');
}

//=============================================================================
//Simulation
//=============================================================================

Simulator.prototype.microstep = function() {
    this.tickCount++;
    //Simulation limit
    if(this.tickCount >= this.end) {
        this.clearHighlights();
        this.canvasExpose();
        this.pause();
    }
    
    var rem = this.tickCount % this.macroStep;
    var steps = this.tickCount / this.macroStep
    
    // Preload phase
    if(this.tickCount == 1) { 
        $('#macrostep1_tx').addClass('highlight');
        this.par2Ser.par2Ser('preload', this.byteEntry1.byteEntry('getValue'));
        $('#par2ser_1 td').addClass('highlight');
    }
    //Receiver collect
    if(this.tickCount == this.end - 2) {
        $('#macrostep4_tx').removeClass('highlight');
        $('#macrostep3_rx').removeClass('highlight');
        $('#macrostep4_rx').addClass('highlight');
        var rxreg = this.ser2Par.ser2Par('getValue');
        this.byteEntry2.byteEntry('setValue', rxreg);
    }
    // Clock Edges
    else if(rem == 0) {
        if(steps == 1) {      //Start bit (Rising Edge)
            $('#macrostep1_tx').removeClass('highlight');
            $('#macrostep2_tx').addClass('highlight');
            $('#par2ser_1 td').removeClass('highlight');
            this.sendOne();
            $('#macrostep1_rx').addClass('highlight');
        }
        else if(steps == 2) { //First bit
            $('#macrostep2_tx').removeClass('highlight');
            $('#macrostep3_tx').addClass('highlight');
            $('#par2ser_1 td').addClass('highlight');
            $('#macrostep1_rx').removeClass('highlight');
            $('#macrostep2_rx').addClass('highlight');
        }
        else if(steps == 18) { //Stop bit
            $('#macrostep3_tx').removeClass('highlight');
            $('#macrostep4_tx').addClass('highlight');
            $('#par2ser_1 td').removeClass('highlight');
            this.sendZero();
            $('#macrostep2_rx').removeClass('highlight');
            $('#macrostep3_rx').addClass('highlight');
        }
        var cycle = this.clock.getValue(this.tickCount);
        if(cycle == 1) { //Rising edge
            this.clkWaveData.push([this.tickCount, cycle, '#FF0000']);
            $('#par2ser_1 td').removeClass('highlight');
            if(steps > 1) {
                //Sender receives info
                var last = this.rxWaveData[this.rxWaveData.length - 1];
                this.ser2Par.ser2Par('clock', last[1]);
                $('#ser2par_1 td').addClass('highlight');
            }
        }
        else { //Falling edge
            this.clkWaveData.push([this.tickCount, cycle, '#008000']);
            if(steps > 1 && steps < 18) { //[start, 8 bits, stop]
                var currVal = this.par2Ser.par2Ser('getBinString')[7];
                if(currVal == '1')
                    this.sendOne();
                else
                    this.sendZero();
                $('#par2ser_1 td').addClass('highlight');
                $('#ser2par_1 td').removeClass('highlight');
                this.par2Ser.par2Ser('clock');
            }
        }
    }
    this.clkWave.waveform('plot', this.tickCount);
    this.txWave.waveform('plot', this.tickCount);
    this.rxWave.waveform('plot', this.tickCount);
    this.updateSensivity();
}
