#!/usr/bin/env python
# -*- coding: utf8 -*-
# minifier.py: CSS and JS minifier

__author__ = "Leandro Resende Mattioli <leandro.mattioli@gmail.com>"

import sys
import os
import os.path
import re
import tempfile
from LRMExtender.log import Log
from jsmin import *

class AbstractMediaMinifier(object):
    """Abstract class for a minifier"""
    def __init__(self):
        self.source_files = []
        self.target_file = ""

    def get_sources_by_dir(self, dir, extension):
        """Chooses all files with <extension> in <dir> as source files"""
        try:
            files = os.listdir(dir)
        except OSError:
            print("Directory %s does not exists!" % dir)
            return False
        pattern = r".*\.%s$" % extension
        sources = [os.path.join(dir,f) for f in files if re.match(pattern, f)]
        self.source_files = sources
        return True

    def add_source(self, path):
        """Adds the source file with path <dir>+<file>"""
        self.source_files.append(path)

    def minify(self):
        """Minify source files outputting to <target_file>"""
        pass

class CSSMinifier(AbstractMediaMinifier):
    """CSS Minifier, using csstidy utility"""
    def __init__(self):
        AbstractMediaMinifier.__init__(self)
        self.csstidy_params = [
            "--compress_colors=true",
            "--compress_font=true",
            "--remove_bslash=true",
            "--remove_last_\;=true",
            "--sort_properties=true"
            "--sort-selectors=false",
            "--template=high"
        ]

    def minify(self):
        """Minify CSS source files outputting to <target_file>"""
        if self.source_files and self.target_file:
            css = ""
            for f in self.source_files:
                css += open(f).read()
            params = " ".join(self.csstidy_params)
            outfile = self.target_file
            tmpFileName = os.path.join(self.target_file + '.tmp')
            tmpFile = open(tmpFileName, 'w')
            tmpFile.write(css)
            tmpFile.close()
            command = "csstidy %s %s %s" % (tmpFileName, params, outfile)
            result = os.system(command)
            os.remove(tmpFileName)
            return result
        else:
            return False



class JSMinifier(AbstractMediaMinifier):
    """Javascript Minifier, using jsmin module"""
    def __init__(self):
        AbstractMediaMinifier.__init__(self)

    def minify(self):
        if self.source_files and self.target_file:
            content = ""
            for f in self.source_files:
                content += open(f).read()
            try:
                open(self.target_file, "w").write(jsmin(content))
                return True
            except IOError:
                return False
        else:
            return False

class ShakeMinifier:
    """ShakeMinifier: higher-level interface for minimizing medias"""
    
    def __init__(self, logpath):
        self.cssDir = None
        self.cssMinDir = None
        self.jsDir = None
        self.jsMinDir = None
        self.CSSRules = {}
        self.JSRules = {}
        self.log = Log(logpath)
    
    def minifyRules(self, type):
        #Preparing specific variables
        if type == 'CSS':
            title = 'Minimizing CSS files'
            minifierClass = CSSMinifier
            path = self.cssDir
            minPath = self.cssMinDir
            rules = self.CSSRules
        elif type == 'JS':
            title = 'Minimizing CSS files'
            minifierClass = JSMinifier
            path = self.jsDir
            minPath = self.jsMinDir
            rules = self.JSRules
        else:
            raise Exception('Invalid type %s: ' % type)
        #Processing
        self.log.title(title)
        for output, sources in rules.iteritems():
            self.log.append('Generating %s' % output)
            min = minifierClass()
            min.target_file = os.path.join(minPath, output)
            for s in sources:
                self.log.append('Reading %s' % s, 1)
                min.add_source(os.path.join(path, s))
            self.log.append('Creating minimized file ...')
            min.minify()

    def minifyCSSRules(self):
        self.minifyRules('CSS')
    
    def minifyJSRules(self):
        self.minifyRules('JS')
