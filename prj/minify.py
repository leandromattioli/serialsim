#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path
import re
from minifier.shakeminifier import ShakeMinifier
from slimmer import html_slimmer

devel = '../devel'
prod = '../prod'
log = '../log'

outputJS = 'js_min/general.min.js'
outputCSS = 'css_min/general.min.css'
outputHTML = 'index.min.html'
inputJS = []
inputCSS = []
newHTMLLines = []

#HTML

with open(path.join(devel, 'index.html')) as f:
    minJS = '<script type="application/javascript" src="%s"></script>'
    minCSS = '<link rel="stylesheet" type="text/css" href="%s" />'
    for l in f.readlines():
        #Scripts
        m = re.search('<script(.*)src="(?P<fName>.*)"(.*)>', l)
        if m:
            fName = m.group('fName')
            inputJS.append(fName)
            continue
        #Stylesheets
        m = re.search('<link (.*)rel="stylesheet" (.*)href="(?P<fName>.*)"(.*)>', l) 
        if m:
            fName = m.group('fName')
            inputCSS.append(fName)
            continue
        #Other lines
        if '</title>' in l:
            newHTMLLines.append(l)
            newHTMLLines.append(minCSS % outputCSS)
            newHTMLLines.append(minJS % outputJS)
        else:
            newHTMLLines.append(l)
            
html = html_slimmer(" ".join(newHTMLLines))
with open(path.join(prod, outputHTML), 'w') as f:
    f.write(html)

#CSS and JavaScript

minifier = ShakeMinifier(path.join(log, 'minifier.log'))
minifier.cssDir = devel
minifier.cssMinDir = prod
minifier.jsDir = devel
minifier.jsMinDir = prod

cssRules = { outputCSS: inputCSS }
jsRules = { outputJS: inputJS }

minifier.CSSRules = cssRules
minifier.JSRules = jsRules
minifier.minifyCSSRules()
minifier.minifyJSRules()
